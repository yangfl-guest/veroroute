/*
	VeroRoute - Qt based Veroboard/Perfboard/PCB layout & routing application.

	Copyright (C) 2017  Alex Lawrow    ( dralx@users.sourceforge.net )

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "renderingdialog.h"
#include "ui_renderingdialog.h"
#include "mainwindow.h"

RenderingDialog::RenderingDialog(QWidget* parent)
: QWidget(parent)
, ui(new Ui_RenderingDialog)
, m_pMainWindow(nullptr)
{
	ui->setupUi( reinterpret_cast<QDialog*>(this) );
	ui->antiAliasOn->setChecked(true);

}

RenderingDialog::~RenderingDialog()
{
	delete ui;
}

void RenderingDialog::SetMainWindow(MainWindow* p)
{
	m_pMainWindow = p;
	QObject::connect(ui->antiAliasOff,		SIGNAL(toggled(bool)),		m_pMainWindow,	SLOT(SetAntialiasOff(bool)));
	QObject::connect(ui->antiAliasOn,		SIGNAL(toggled(bool)),		m_pMainWindow,	SLOT(SetAntialiasOn(bool)));
	QObject::connect(ui->spinBox_bright,	SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetBrightness(int)));
	QObject::connect(ui->shade,				SIGNAL(toggled(bool)),		m_pMainWindow,	SLOT(SetShowTarget(bool)));
	QObject::connect(ui->comptext,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetTextSizeComp(int)));
	QObject::connect(ui->comppins,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetTextSizePins(int)));
	QObject::connect(ui->spinBox_height,	SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetTargetRows(int)));
	QObject::connect(ui->spinBox_width,		SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetTargetCols(int)));
	QObject::connect(ui->padWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetPadWidth(int)));
	QObject::connect(ui->trackWidth,		SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetTrackWidth(int)));
	QObject::connect(ui->tagWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetTagWidth(int)));
	QObject::connect(ui->holeWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetHoleWidth(int)));
	QObject::connect(ui->gapWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetGapWidth(int)));
	QObject::connect(ui->maskWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetMaskWidth(int)));
	QObject::connect(ui->silkWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetSilkWidth(int)));
	QObject::connect(ui->edgeWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetEdgeWidth(int)));
	QObject::connect(ui->fillWidth,			SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetFillWidth(int)));
	QObject::connect(ui->viapadWidth,		SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetViaPadWidth(int)));
	QObject::connect(ui->viaholeWidth,		SIGNAL(valueChanged(int)),	m_pMainWindow,	SLOT(SetViaHoleWidth(int)));
	QObject::connect(ui->Xthermals,			SIGNAL(toggled(bool)),		m_pMainWindow,	SLOT(SetXthermals(bool)));
	QObject::connect(ui->closeTracks,		SIGNAL(toggled(bool)),		m_pMainWindow,	SLOT(SetShowCloseTracks(bool)));
}


void RenderingDialog::UpdateControls()
{
	Board& board = m_pMainWindow->m_board;

	double minTrk(0), minGnd(0);
	board.GetSeparations(minTrk, minGnd);	// Put be called first since it may clear the warning points

	const bool bCompEdit		= board.GetCompEdit();
	const bool bPCB				= board.GetTrackMode() == TRACKMODE::PCB;
	const bool bMonoPCB			= board.GetTrackMode() == TRACKMODE::MONO || bPCB;
	const bool bNoTrackOptions	= board.GetTrackMode() == TRACKMODE::OFF;
	const bool bVero			= board.GetVeroTracks();
	const bool bVias			= board.GetViasEnabled();
	const bool bCloseTrackInfo	= board.GetHaveWarnPoints();
	const bool bGroundFill		= !bVero && bMonoPCB && board.GetGroundFill();
	const bool bTags			= !bCompEdit && !bNoTrackOptions && bGroundFill;

	const int ii =  board.GetBackgroundColor().GetR();	// Before VeroRoute V2.20, "Brightness" was stored as a grey level in the range [200,255].
	const int i  = 5 * ( ( ii - 155 ) / 5 );			// From   VeroRoute V2.20, "Brightness" is a percentage value [0,100] that is stored as a grey level in the range [155,255].

	ui->spinBox_bright->setValue( i );
	ui->comptext->setValue( board.GetTextSizeComp() );
	ui->comppins->setValue( board.GetTextSizePins() );
	ui->spinBox_height->setValue( board.GetTargetRows() );
	ui->spinBox_width->setValue( board.GetTargetCols() );
	ui->shade->setChecked( board.GetShowTarget() );
	if ( board.GetRenderQuality() == 0 ) ui->antiAliasOff->setChecked(true); else ui->antiAliasOn->setChecked(true);

	ui->spinBox_bright->setDisabled(bCompEdit || bPCB );
	ui->shade->setDisabled(	bCompEdit );
	ui->spinBox_height->setDisabled(bCompEdit );
	ui->spinBox_width->setDisabled(	bCompEdit );
	ui->comptext->setDisabled(		bCompEdit );
	ui->comppins->setDisabled(		!bCompEdit && bMonoPCB );
	ui->padWidth->setDisabled(		bCompEdit || bNoTrackOptions || bVero );
	ui->trackWidth->setDisabled(	bCompEdit || bNoTrackOptions || bVero );
	ui->tagWidth->setDisabled(		!bTags );
	ui->holeWidth->setDisabled(		bCompEdit || bNoTrackOptions || bVero );
	ui->viapadWidth->setDisabled(	bCompEdit || bNoTrackOptions || bVero || !bVias );
	ui->viaholeWidth->setDisabled(	bCompEdit || bNoTrackOptions || bVero || !bVias );
	ui->gapWidth->setDisabled(		bCompEdit || !bGroundFill );
	ui->maskWidth->setDisabled(		bCompEdit || bVero || !bPCB );
	ui->silkWidth->setDisabled(		bCompEdit || bVero || !bMonoPCB );
	ui->edgeWidth->setDisabled(		bCompEdit );
	ui->fillWidth->setDisabled(		bCompEdit || !bGroundFill );

	// ... and corresponding labels
	ui->label_brightness->setDisabled(	bCompEdit || bPCB );
	ui->label_height->setDisabled(		bCompEdit );
	ui->label_width->setDisabled(		bCompEdit );
	ui->label_comptext->setDisabled(	bCompEdit );
	ui->label_comppins->setDisabled(	!bCompEdit && bMonoPCB );
	ui->label_pad->setDisabled(			bCompEdit || bNoTrackOptions || bVero );
	ui->label_track->setDisabled(		bCompEdit || bNoTrackOptions || bVero );
	ui->label_tag->setDisabled(			!bTags );
	ui->label_hole->setDisabled(		bCompEdit || bNoTrackOptions || bVero );
	ui->label_viapad->setDisabled(		bCompEdit || bNoTrackOptions || bVero || !bVias );
	ui->label_viahole->setDisabled(		bCompEdit || bNoTrackOptions || bVero || !bVias );
	ui->label_gap->setDisabled(			bCompEdit || !bGroundFill );
	ui->label_mask->setDisabled(		bCompEdit || bVero || !bPCB );
	ui->label_silk->setDisabled(		bCompEdit || bVero || !bMonoPCB );
	ui->label_edge->setDisabled(		bCompEdit );
	ui->label_fill->setDisabled(		bCompEdit || !bGroundFill );
	ui->label_info->setDisabled(		bCompEdit || bVero );
	ui->label_info_2->setDisabled(		bCompEdit || !bGroundFill );
	ui->Xthermals->setDisabled(			!bTags );
	if ( !bTags )
		ui->Xthermals->hide();
	else
		ui->Xthermals->show();
	ui->closeTracks->setDisabled(		bCompEdit || bVero || !bCloseTrackInfo );
	if ( bCompEdit || bVero || !bCloseTrackInfo )
		ui->closeTracks->hide();
	else
		ui->closeTracks->show();

	ui->padWidth->setValue(		board.GetPAD_MIL()		);
	ui->trackWidth->setValue(	board.GetTRACK_MIL()	);
	ui->tagWidth->setValue(		board.GetTAG_MIL()		);
	ui->holeWidth->setValue(	board.GetHOLE_MIL()		);
	ui->gapWidth->setValue(		board.GetGAP_MIL()		);
	ui->maskWidth->setValue(	board.GetMASK_MIL()		);
	ui->silkWidth->setValue(	board.GetSILK_MIL()		);
	ui->edgeWidth->setValue(	board.GetEDGE_MIL()		);
	ui->fillWidth->setValue(	board.GetFILL_MIL()		);
	ui->viapadWidth->setValue(	board.GetVIAPAD_MIL()	);
	ui->viaholeWidth->setValue(	board.GetVIAHOLE_MIL()	);
	ui->Xthermals->setChecked(	board.GetXthermals()	);

	const int minTrkMil = static_cast<int>(minTrk);
	const int minTrkRem = static_cast<int>(10.0 * (minTrk - minTrkMil));	// 0.1 mil resolution
	const int minGndMil = static_cast<int>(minGnd);
	const int minGndRem = static_cast<int>(10.0 * (minGnd - minGndMil));	// 0.1 mil resolution

	std::string str = "Current min track separation = ";
	if ( bCompEdit || bVero )
		str += "n/a";
	else
		str += std::to_string(minTrkMil) + "." + std::to_string(minTrkRem) + " mil";
	ui->label_info->setText( QString::fromStdString(str) );

	std::string str2 = "Current min ground-fill width = ";
	if ( bCompEdit || !bGroundFill )
		str2 += "n/a";
	else
		str2 += std::to_string(minGndMil) + "." + std::to_string(minGndRem) + " mil";
	ui->label_info_2->setText( QString::fromStdString(str2) );

	ui->closeTracks->setChecked( board.GetShowCloseTracks() );
}

void RenderingDialog::keyPressEvent(QKeyEvent* event)
{
#ifndef VEROROUTE_ANDROID
	m_pMainWindow->specialKeyPressEvent(event);
#endif
	QWidget::keyPressEvent(event);
	event->accept();
}

void RenderingDialog::keyReleaseEvent(QKeyEvent* event)
{
#ifdef VEROROUTE_ANDROID
	if ( event->key() == Qt::Key_Back )
		return m_pMainWindow->keyReleaseEvent(event);	// Try Undo operation
#else
	m_pMainWindow->commonKeyReleaseEvent(event);
#endif
	QWidget::keyReleaseEvent(event);
	event->accept();
}
