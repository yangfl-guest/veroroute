/*
	VeroRoute - Qt based Veroboard/Perfboard/PCB layout & routing application.

	Copyright (C) 2017  Alex Lawrow    ( dralx@users.sourceforge.net )

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

// Comment out following 3 lines on debug builds to enable assert()
#ifndef NDEBUG
#define NDEBUG
#endif

// Uncomment next line to allow test SOICs using component editor
//#define _TEST_SOIC

// Uncomment next line to allow grid debug info via F4 key
//#define _GRID_DEBUG

#include <cmath>
#include <stdio.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ios>
#include <assert.h>
#include <cfloat>
#include <climits>
#include <algorithm>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <chrono>
#include "qcompilerdetection.h"	// For Q_DECL_CONSTEXPR

Q_DECL_CONSTEXPR static const double DBL_PI				= 3.14159265358979323846;
Q_DECL_CONSTEXPR static const double DBL_PI_4			= DBL_PI / 4.0;
Q_DECL_CONSTEXPR static const double RADIANS_PER_DEGREE	= DBL_PI / 180.0;
Q_DECL_CONSTEXPR static const double DEGREES_PER_RADIAN	= 180.0 / DBL_PI;
