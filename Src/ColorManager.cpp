/*
	VeroRoute - Qt based Veroboard/Perfboard/PCB layout & routing application.

	Copyright (C) 2017  Alex Lawrow    ( dralx@users.sourceforge.net )

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ColorManager.h"

MyRGB ColorManager::sm_color[] = { MyRGB(0x3C18C8), MyRGB(0xC024F8), MyRGB(0xD26060), MyRGB(0xDCDC58)
								 , MyRGB(0x60C858), MyRGB(0x5896C8), MyRGB(0x6010FF), MyRGB(0xE142D2)
								 , MyRGB(0xE18C30), MyRGB(0xA0C828), MyRGB(0x48C896), MyRGB(0x5080FF) };
